﻿using UnityEngine;

//Floor will consist multiple rooms.
[System.Serializable]
public class Floor
{
    public int FloorNumber
    {
        get;
        private set;
    }

    [SerializeField]
    public Room[,] rooms;

    public Floor(int floorNumber, Room[,] rooms)
    {
        this.FloorNumber = floorNumber;
        this.rooms = rooms;
    }
}