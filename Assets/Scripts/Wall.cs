﻿[System.Serializable]
public class Wall
{
    //Wall types could be normal, door, with balcony etc.
    public enum WallType
    {
        Normal
    }

    //Property - by default set to normal.
    public WallType WallTypeSelected
    {
        get;
        private set;
    } = WallType.Normal;

    //Constructor - by default set to normal.
    public Wall (WallType wallType = WallType.Normal)
    {
        this.WallTypeSelected = wallType;
    }
}