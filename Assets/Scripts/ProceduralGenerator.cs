﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ProceduralGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject wallPrefab;

    [SerializeField]
    private GameObject roofPrefab;

    [SerializeField]
    private bool includeRoof = false;

    [SerializeField]
    private int width = 3, height = 3;

    [SerializeField]
    private float cellUnitSize = 1;

    [SerializeField]
    private int numberOfFloors = 1;

    [SerializeField]
    private Floor[] floors;

    private void Awake()
    {
        // Creates the data structures.
        Generate();

        // Spawns prefabs.
        Render();
    }

    private void Generate()
    {
        floors = new Floor[numberOfFloors];

        int floorCount = 0;

        foreach(Floor floor in floors)
        {
            Room[,] rooms = new Room[width, height];

            for(int i = 0; i < width; i++)
            {
                for(int j = 0; j < height; j++)
                {
                    if(includeRoof)
                    {
                        floorCount = floors.Length - 1;
                    }

                    rooms[i, j] = new Room(new Vector2(i * cellUnitSize, j * cellUnitSize), includeRoof);
                }
            }

            floors[floorCount] = new Floor(floorCount++, rooms);
        }
    }

    private void Render()
    {

    }
}