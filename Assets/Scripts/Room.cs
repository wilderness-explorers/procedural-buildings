﻿using UnityEngine;

//Room - composed of multiple walls.
public class Room
{
    public Wall[] walls;

    private Vector2 position;

    //Buildings with multiple floors will have rooms with floor instead of roof object.
    public bool HasRoof
    {
        get;
        private set;
    }

    public Room (Vector2 position, bool hasRoof = false)
    {
        this.position = position;
        this.HasRoof = hasRoof;
    }

    public Vector2 RoomPosition
    {
        get
        {
            return this.position;
        }
    }
}